package com.example.firstkotlinapp

import java.lang.*
import java.lang.Double.parseDouble
import java.util.*


class ExpressionCalculator {

    private fun isOperationSymbol(symbol: Char): Boolean {
        return symbol.equals('*') || symbol.equals('/') || symbol.equals('+') || symbol.equals('-');
    }

    private fun multiply(left: String, right: String): String {
        val leftD = parseDouble(left)
        val rightD = parseDouble(right)
        val result = leftD * rightD
        return result.toString()
    }

    private fun plus(left: String, right: String): String {
        val leftD = parseDouble(left)
        val rightD = parseDouble(right)
        val result = leftD + rightD
        return result.toString()
    }

    private fun minus(left: String, right: String): String {
        val leftD = parseDouble(left)
        val rightD = parseDouble(right)
        val result = rightD - leftD
        return result.toString()
    }

    private fun divide(left: String, right: String): String {
        val leftD = parseDouble(left)
        val rightD = parseDouble(right)
        if (leftD.equals(0.0)) {
            return "0"
        } else {
            val result = rightD / leftD
            return result.toString()
        }
    }

    private fun convertToReversePolishNotation(expr: String): String {
        val exprArr: CharArray = expr.toCharArray()
        val stack = Stack<Char>()
        val out = StringBuilder()

        for (i in exprArr.indices) {
            when (exprArr[i]) {
                '+', '-' -> {
                    while (!stack.empty() && (stack.peek() == '*' || stack.peek() == '/')) {
                        out.append(' ');
                        out.append(stack.pop());
                    }
                    out.append(' ')
                    stack.push(exprArr[i])
                }
                '*', '/' -> {
                    out.append(' ')
                    stack.push(exprArr[i])
                }
                else -> {
                    out.append(exprArr[i])
                }
            }
        }

        while (!stack.isEmpty()) {
            out.append(' ')
            out.append(stack.pop())
        }

        return out.toString()
    }

    private fun calcReversePolishNotation(expr: String): String {
        val exprList: List<String> = expr.split(' ');
        val stack = Stack<String>()
        for (i in exprList.indices) {
            if (exprList.get(i).length == 1 && isOperationSymbol(exprList.get(i)[0])) {
                when (exprList.get(i)) {
                    "*" -> stack.push(multiply(stack.pop(), stack.pop()))
                    "/" -> stack.push(divide(stack.pop(), stack.pop()))
                    "+" -> stack.push(plus(stack.pop(), stack.pop()))
                    "-" -> stack.push(minus(stack.pop(), stack.pop()))
                }
            } else {
                stack.push(exprList.get(i))
            }
        }
        return stack.pop();
    }

    fun calculate(expr: String): String {
        val polishExpr: String = convertToReversePolishNotation(expr)
        return calcReversePolishNotation(polishExpr);
    }
}