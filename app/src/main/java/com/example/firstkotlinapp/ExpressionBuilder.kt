package com.example.firstkotlinapp

class ExpressionBuilder {

    private lateinit var callback: (String) -> Unit
    private val stringBuilder: StringBuilder = StringBuilder();
    private var afterResult: Boolean = false;

    private fun isOperationSymbol(symbol: Char): Boolean {
        return symbol.equals('*') || symbol.equals('/') || symbol.equals('+') || symbol.equals('-');
    }

    fun setCallback(callback: (String) -> Unit) {
        this.callback = callback
    }

    fun printText() {
        this.callback(stringBuilder.toString());
    }

    fun clear() {
        this.stringBuilder.clear()
        printText()
    }

    fun delete() {
        if (this.stringBuilder.length > 0) {
            this.stringBuilder.deleteCharAt(this.stringBuilder.length - 1)
            printText()
        }
    }

    fun calc() {
        val calculator: ExpressionCalculator = ExpressionCalculator()
        val result: String = calculator.calculate(stringBuilder.toString())
        this.stringBuilder.clear()
        this.stringBuilder.append(result)
        this.afterResult = true;
        printText()
    }

    fun addSymbol(symbol: String) {
        if (this.afterResult) {
            this.stringBuilder.clear()
            this.afterResult = false;
        }
        val char: Char = symbol.toCharArray()[0];
        if (this.stringBuilder.length > 0 && char.equals('=')) {
            if (isOperationSymbol(this.stringBuilder.last())) {
                this.stringBuilder.deleteCharAt(this.stringBuilder.length - 1)
            }
            calc()
            return
        } else if (isOperationSymbol(char)) {
            if (this.stringBuilder.length == 0 ) {
                return
            }
            if (isOperationSymbol(this.stringBuilder.last())) {
                this.stringBuilder.deleteCharAt(this.stringBuilder.length - 1)
            }
        }
        this.stringBuilder.append(symbol);
        printText()
    }
}