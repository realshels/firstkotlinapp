package com.example.firstkotlinapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private val expressionBuilder = ExpressionBuilder();

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        expressionBuilder.setCallback(::outText);
    }

    fun outText(text: String): Unit {
        textView.text = text;
    }

    fun symbolOnClick(view: View) {
        val btn: Button = view as Button
        expressionBuilder.addSymbol(btn.text.toString());
    }

    fun clearOnClick(view: View) {
        expressionBuilder.clear()
    }

    fun deleteOnClick(view: View) {
        expressionBuilder.delete()
    }
}
